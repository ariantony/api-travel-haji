<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use App\Http\Helper\ResponseBuilderList;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    public $successStatus = 200;

    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $token = $user->createToken('nApp')->accessToken;
            $privileges = $user->getAllPermissions();

            return response()->json([
                'token'         => $token,
                'profile'         => $user,
                'permissions'    => $privileges

            ],$this->successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email','unique:users,email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('nApp')->accessToken;
        $success['name'] =  $user->name;

        //$user->assignRole('writer');

        return response()->json(['success'=>$success], $this->successStatus);
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }



    public function do()
    {
       return 'belum login';
    }

    public function index() {
        $data = User::all();
        $status = true;
        $message  = "Data  ditemukan.";
        $response_code = Response::HTTP_OK;
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);
    }

    public function show($id)
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = User::find($id);

        if (empty($data)){
            $message  = "Data tidak ditemukan";
            return ResponseBuilder::result('False', $message, '[]', '404');
        }

        return ResponseBuilder::result($status, $message, $data, $response_code);
    }

    public function update(Request $request, $id)
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_CREATED;
        $data = User::find($id);

        if (empty($data)){
                $message  = "ID tidak ditemukan";
                return ResponseBuilder::result('False', $message, '[]', '404');
        }

        $data = [];
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['created_at'] = now();
        $data['updated_at'] = now();


        $update = User::where('id','=', $id)
                ->update($data);

        $response = [
            'message'=>'Data successfully update.',
            'status'=> $update,
            'data' => $data
        ];


        return ResponseBuilder::result($status, $message, $data, $response_code);

    }

}
