<?php

namespace App\Http\Controllers;

use App\Models\Universities;
use Illuminate\Http\Request;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;


class UniversityContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = Universities::paginate(15);

        return ResponseBuilder::result($status, $message, $data, $response_code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $validator = Validator::make($request->all(),[
        //     'name' => ['required']
        // ]);

        // if($validator->fails()){
        //     return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        // }

        // $data = $request->all();

        // foreach ($data as $key => $value) {
        //     return dd($key, $value);
        // }

            // Universities::create([
            //     'name'     => $item['name'],
            //     'description'     => $item['description'],
            //     'created_at'   => now(),
            //     'updated_at'   => now()
            // ]);

        $data = [];
        $data['name'] = $request->input('name');
        $data['description'] = $request->input('description');
        $data['created_at'] = now();
        $data['updated_at'] = now();

        $insert_data = Universities::insert($data);

        $response = [
            'message'=>'Data successfully inserted.',
            'insert_data_status'=> $insert_data
        ];

        return response()->json($response, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = Universities::find($id);

        if (empty($data)){
            $message  = "Data tidak ditemukan";
            return ResponseBuilder::result('False', $message, '[]', '404');
        }

        return ResponseBuilder::result($status, $message, $data, $response_code);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = true;
        $message  = "Data successfully update.";
        $response_code = Response::HTTP_CREATED;
        $data = Universities::find($id);

        if (empty($data)){
                $message  = "ID tidak ditemukan";
                return ResponseBuilder::result('False', $message, '[]', '404');
        }

        $data = [];
        $data['name'] = $request->input('name');
        $data['description'] = $request->input('description');
        $data['updated_at'] = now();

        $update = Universities::where('id','=', $id)
                ->update($data);

        return ResponseBuilder::result($status, $message, $data, $response_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = true;
        $message  = "Data berhasil di hapus";
        $response_code = Response::HTTP_OK;
        $data = Universities::find($id);

        if (empty($data)){
                $message  = "ID tidak ditemukan";
                return ResponseBuilder::result('False', $message, '[]', '404');
            }

        $data->delete();

        return ResponseBuilder::result($status, $message, $data, $response_code);
    }
}
