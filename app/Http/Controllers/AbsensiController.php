<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use Illuminate\Database\Schema\ForeignKeyDefinition;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AbsensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $test = $request->user()->hasRole('admin');
        dd($test);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        foreach($data as $x => $val_x) {
            $postdata = [];

            $postdata['id_log'] = $val_x['id'];
            $postdata['id_fp'] = $val_x['id_fp'];
            $postdata['pin'] = $val_x['pin'];
            $postdata['datetime'] = $val_x['datetime'];
            $postdata['verified'] = $val_x['verified'];
            $postdata['status'] = $val_x['status'];
            $postdata['created_at'] = now();
            $postdata['created_by'] = 'api';

            $insert_data = Absensi::insertOrIgnore($postdata);
        }

        $response = [
            'message'=>'Data successfully inserted.',
            'insert_data_status'=> $insert_data
        ];

        return response()->json($response, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function postdata(Request $request)
    {
        $response = [
            'message'=>'Data successfully inserted.',
            'insert_data_status'=> $request
        ];
        return response()->json($response, Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
