<?php

namespace App\Http\Controllers\Contoh;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;

class ContohModulController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware(['role:admin'])->only(['index', 'store']);
    // }
    /**
     * Display a listing of the resource.
     * , ['only' => ['fooAction', 'barAction']])
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(2);
        //$user = User::find(1); #ini yang bener
        //$role = Role::create(['name' => 'writer']);
        // $permission = Permission::create(['name' => 'edit articles']);
        //$role = Role::find(1)->name;

        //dd($role);

        // $role->givePermissionTo($permission);
        // $permission->assignRole($role);

        //$user->hasRole('writer');
        //$user->assignRole($role);

        // return 'index';

        Permission::create(['name'=>'posts.*']);
        $user->givePermissionTo('posts.*');

        //Permission::create(['name'=>'posts.*']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return 'store';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'show';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return 'update';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return 'delete';
    }
}
