<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncAbsensi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absensi:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload Data Absensi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo "test command";
    }
}
