<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleHasPermission extends Model
{
    use HasFactory;

    public $table = 'role_has_permissions';
    protected $fillable = ['permission_id','role_id'];
    public $timestamps = false;
    public $incrementing = false;

    protected $guarded = [];
    protected $hidden = [];
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];
}
