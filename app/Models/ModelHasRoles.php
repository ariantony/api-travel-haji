<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelHasRoles extends Model
{
    use HasFactory;
    public $table = 'model_has_roles';
    protected $fillable = ['permission_id','role_id','model_type'];
    public $timestamps = false;
    public $incrementing = false;

    protected $guarded = [];
    protected $hidden = [];
    protected $casts = [];
}
