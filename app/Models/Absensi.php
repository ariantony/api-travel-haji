<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    use HasFactory;


    public $table = 'tbl_log';
    public $timestamps = false;

    protected $fillable = [
        'id_log',
        'id_fp',
        'pin',
        'datetime',
        'verified',
        'status',
        'created_at',
        'created_by'
    ];

    protected $guarded = [

    ];

    protected $hidden = [

    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'datetime' => 'datetime:Y-m-d H:i:s'
    ];
}
